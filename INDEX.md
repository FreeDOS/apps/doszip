# Doszip Commander

A small LFN-aware file manager with built-in (un)zipper. It is built with JWasm and OpenWatcom. Includes full sources for DOS and Windows

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## DOSZIP.LSM

<table>
<tr><td>title</td><td>Doszip Commander</td></tr>
<tr><td>version</td><td>2.66</td></tr>
<tr><td>entered&nbsp;date</td><td>2024-03-06</td></tr>
<tr><td>description</td><td>LFN-aware file manager with built-in (un)zipper.</td></tr>
<tr><td>summary</td><td>A small LFN-aware file manager with built-in (un)zipper. It is built with JWasm and OpenWatcom. Includes full sources for DOS and Windows</td></tr>
<tr><td>keywords</td><td>DOS FreeDOS</td></tr>
<tr><td>author</td><td>Hjort Nidudsson (nidud _AT_ users.sourceforge.net)</td></tr>
<tr><td>maintained&nbsp;by</td><td>Hjort Nidudsson (nidud _AT_ users.sourceforge.net)</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/nidud/doszip</td></tr>
<tr><td>original&nbsp;site</td><td>http://sourceforge.net/projects/doszip/</td></tr>
<tr><td>mirror&nbsp;site</td><td>http://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/util/file/doszip/</td></tr>
<tr><td>platforms</td><td>DOS WIN32</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
</table>
